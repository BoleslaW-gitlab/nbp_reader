#! python3

import requests
from requests.exceptions import HTTPError
import time

# script parameters
POLL_INTERVAL = 10
CURRENCIES = ('USD', 'EUR')

kurs_sredni_url = "http://api.nbp.pl/api/exchangerates/rates/A/{currency}/last/1/?format=JSON"

last_rates = {n: 0.0 for n in CURRENCIES}

# http://api.nbp.pl/api/exchangerates/rates/c/usd/today/?format=json
# http://api.nbp.pl/api/exchangerates/tables/A/?format=json


def process_data(single_rate):
    global last_rates

    currency = single_rate['code']
    rate = {currency: single_rate['rates'][0]['mid']}
    if rate[currency] != last_rates[currency]:
        print(f"{currency} rate changed. New rate {currency}: {rate[currency]}")
    last_rates.update(rate)

    # print(next(iter(rate)))   # outputs 'key' of dict


def main():
    while True:
        for currency in CURRENCIES:
            url = kurs_sredni_url.format(currency=currency)
            try:
                response = requests.get(url)
                # If the response was successful, no Exception will be raised
                response.raise_for_status()
            except HTTPError as http_err:
                print(f'HTTP error occurred: {http_err}')
            except Exception as err:
                print(f'Other error occurred: {err}')
            else:
                # print(response.content)
                # print(response.json()['rates'][0]['mid'])
                process_data(response.json())
        print(last_rates)
        time.sleep(POLL_INTERVAL)


if __name__ == '__main__':
    main()
